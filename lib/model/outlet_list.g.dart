// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'outlet_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OutletModel _$OutletModelFromJson(Map<String, dynamic> json) => OutletModel(
      code: json['code'] as int?,
      message: json['message'] as String?,
      data: (json['data'] as List<dynamic>)
          .map((e) =>
              e == null ? null : DataOutlet.fromJson(e as Map<String, dynamic>))
          .toList(),
      meta: json['meta'] == null
          ? null
          : Meta.fromJson(json['meta'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$OutletModelToJson(OutletModel instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'data': instance.data,
      'meta': instance.meta,
    };

DataOutlet _$DataOutletFromJson(Map<String, dynamic> json) => DataOutlet(
      id: json['id'] as int?,
      imageThumb: json['image_thumb'] as String?,
      name: json['name'] as String?,
      openAtDisplay: json['open_at_display'] as String?,
      statusName: json['status_name'] as String?,
      statusDisabled: json['status_disabled'] as int?,
    );

Map<String, dynamic> _$DataOutletToJson(DataOutlet instance) =>
    <String, dynamic>{
      'id': instance.id,
      'image_thumb': instance.imageThumb,
      'name': instance.name,
      'open_at_display': instance.openAtDisplay,
      'status_name': instance.statusName,
      'status_disabled': instance.statusDisabled,
    };

Meta _$MetaFromJson(Map<String, dynamic> json) => Meta(
      page: json['page'] as int?,
      perPage: json['per_page'] as int?,
      count: json['count'] as int?,
      totalPage: json['total_page'] as int?,
      totalCount: json['total_count'] as int?,
    );

Map<String, dynamic> _$MetaToJson(Meta instance) => <String, dynamic>{
      'page': instance.page,
      'per_page': instance.perPage,
      'count': instance.count,
      'total_page': instance.totalPage,
      'total_count': instance.totalCount,
    };
