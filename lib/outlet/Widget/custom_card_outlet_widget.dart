import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../utils/app_colors.dart';
import '../../utils/size_config.dart';

// ignore: must_be_immutable
class CustomCard extends StatelessWidget {
  const CustomCard(
      {super.key,
      required this.title,
      required this.image,
      required this.time,
      required this.schedule,
      required this.status});

  final String title;
  final String time;
  final String image;
  final String schedule;
  final int? status;

  // String time;
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: AppColors.itemDark,
        ),
        margin: EdgeInsets.symmetric(
            vertical: SizeConfig.vertical(1),
            horizontal: SizeConfig.horizontal(1)),
        width: SizeConfig.screenWidth,
        height: SizeConfig.vertical(13),
        child: wrapAll(context, image, time, title, schedule, status));
  }
}

/// Widget for wrap all content on custom card
Widget wrapAll(BuildContext context, String image, String time, String title,
    String schedule, int? status) {
  return Row(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Container(
        width: SizeConfig.horizontal(30),
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
          image: DecorationImage(
              image: NetworkImage(
                image,
              ),
              fit: BoxFit.cover),
        ),
      ),
      Flexible(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(1)),
          padding: EdgeInsets.only(
              left: SizeConfig.horizontal(1), top: SizeConfig.vertical(1)),
          width: SizeConfig.horizontal(65),
          height: SizeConfig.vertical(15),
          child: Row(
            children: [valueInsideCard(context, title, time, schedule, status)],
          ),
        ),
      ),
    ],
  );
}

/// Widget cardValue
Widget valueInsideCard(BuildContext context, String title, String time,
    String schedule, int? status) {
  return Flexible(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        titleName(context, title),
        SizedBox(
          width: SizeConfig.horizontal(65),
          height: SizeConfig.vertical(7),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(
                time,
                style: GoogleFonts.montserrat(
                    color: AppColors.whiteBackground,
                    fontWeight: FontWeight.bold,
                    fontSize: 12),
              ),
              statusOutlet(context, schedule, status)
            ],
          ),
        )
      ],
    ),
  );
}

/// for title name
Widget titleName(BuildContext context, String title) {
  return Text(
    title,
    style: GoogleFonts.montserrat(
        color: AppColors.lightGold, fontWeight: FontWeight.bold, fontSize: 14),
  );
}

Widget statusOutlet(BuildContext context, String schedule, int? status) {
  // DateTime now = DateTime.now();
  // var minutes = now.minute;
  return Row(
    children: [
      Flexible(
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.horizontal(2),
          ),
          decoration: BoxDecoration(
            color: status == 0 ? AppColors.lightGold : AppColors.greyDisabled,
            borderRadius: BorderRadius.circular(20),
          ),
          height: SizeConfig.vertical(2.5),
          child: Text(
            schedule,
            textAlign: TextAlign.center,
            style: GoogleFonts.montserrat(
                color: AppColors.darkBackground,
                fontSize: 12,
                fontStyle: FontStyle.italic),
          ),
        ),
      )
    ],
  );
}
